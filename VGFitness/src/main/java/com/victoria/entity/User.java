package com.victoria.entity;

import java.time.LocalDate;

public class User {
	
	private String id = "";
	private String email = "";
	private String firstName = "Unknown";
	private String secondName = "Unknown";
	private LocalDate dateOfBirth = LocalDate.MIN;
	private LocalDate enrollmentBeg = LocalDate.MIN;
	private LocalDate enrollmentEnd = LocalDate.MIN;
	private int freezing;
	private int bonus;
	private String pass = "";
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		if (id != null)
			this.id = id;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		if(email != null)
			this.email = email;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		if(firstName != null)
			this.firstName = firstName;
	}
	
	public String getSecondName() {
		return secondName;
	}
	
	public void setSecondName(String secondName) {
		if(secondName != null)
			this.secondName = secondName;
	}
	
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(LocalDate dateOfBirth) {
		if(dateOfBirth != null)
			this.dateOfBirth = dateOfBirth;
	}
	
	public LocalDate getEnrollmentBeg() {
		return enrollmentBeg;
	}
	
	public void setEnrollmentBeg(LocalDate enrollmentBeg) {
		if(enrollmentBeg != null)
			this.enrollmentBeg = enrollmentBeg;
	}
	
	public LocalDate getEnrollmentEnd() {
		return enrollmentEnd;
	}

	public void setEnrollmentEnd(LocalDate enrollmentEn) {
		this.enrollmentEnd = enrollmentEn;
	}
	
	public int getFreezing() {
		return freezing;
	}
	
	public void setFreezing(int freezing) {
		this.freezing = freezing < 0 ? 0 : freezing;
	}
	
	public int getBonus() {
		return bonus;
	}
	
	public void setBonus(int bonus) {
		this.bonus = bonus < 0 ? 0 : bonus;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		if(pass != null)
			this.pass = pass;
	}
}
