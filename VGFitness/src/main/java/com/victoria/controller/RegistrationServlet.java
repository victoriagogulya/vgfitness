package com.victoria.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.victoria.dao.DAOFactory;
import com.victoria.dao.UserDAO;
import com.victoria.dao.mysql.MySQLDAOFactory;
import com.victoria.entity.User;

public final class RegistrationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private enum RegistrationResult { REGISTERED, CANCEL, ERROR_SERVER, ERROR_USER };

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String id = req.getParameter("id");
		String pass1 = req.getParameter("pass1");
		String pass2 = req.getParameter("pass2");
		RegistrationResult result = RegistrationResult.CANCEL;
		
		if(checkParameters(id, pass1, pass2)) {
			User user = new User();
			user.setId(id);
			user.setPass(md5Apache(pass1));
			System.out.println(">>> " + user.getPass());
			DAOFactory factory = new MySQLDAOFactory();
			System.out.println(">>> " + "DAOFactory created");
			try (Connection connection = factory.getConnection()) {
				System.out.println(">>> " + "Connection got");
				UserDAO userDAO = factory.getUserDAO(connection);
				System.out.println(">>> " + "UserDAO got");
				if(userDAO.create(user)) {
					result = RegistrationResult.REGISTERED;
				}
				else {
					result = RegistrationResult.CANCEL;
				}
			} catch (SQLException e) {
				result = RegistrationResult.ERROR_SERVER;
				e.printStackTrace();
			}
		}
		else {
			result = RegistrationResult.ERROR_USER;
		}
		
		generateResponse(result,req, resp);
	}
	
	private boolean checkParameters(String id, String pass1, String pass2) {
		return id != null && pass1 != null && pass2 != null
				&& !id.isEmpty() && !pass1.isEmpty() && !pass2.isEmpty()
				&& pass1.equals(pass2);
	}
	
	private String md5Apache(String pass) {
	    return DigestUtils.md5Hex(pass);
	}
	
	private void generateResponse(RegistrationResult result, HttpServletRequest req, HttpServletResponse resp) {
		String message = "REGISTERED";
		switch(result) {
		case REGISTERED:
			break;
		case CANCEL:
			message = "CANCEL:Извините, но мы не можем Вас зарегистрировать. /n/n" 
			           + "Такого номера карты не существует";
			break;
		case ERROR_USER:
			message = "ERROR_USER:Ошибка! Номер карты или пароль введены не верно.";
			break;
		case ERROR_SERVER:
			message = "ERROR_SERVERОшибка сервера. Извините, за неудобства.";
			break;
		}
		
		req.setAttribute("message", message);
		try {
			req.getRequestDispatcher("/jsp/error.jsp").forward(req, resp);
		} catch (ServletException | IOException e) {
			e.printStackTrace();
		}
	}
	
}
