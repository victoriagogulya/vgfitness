package com.victoria.dao;

import java.sql.Connection;
import java.sql.SQLException;


public interface DAOFactory {
	
	Connection getConnection() throws SQLException;
	
	UserDAO getUserDAO(Connection connection);
}