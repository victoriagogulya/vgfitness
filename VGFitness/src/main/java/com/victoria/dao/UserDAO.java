package com.victoria.dao;

import java.sql.SQLException;
import java.util.List;

import com.victoria.entity.User;

public interface UserDAO {
	
	boolean create(User user) throws SQLException;
	User getByPK(String key) throws SQLException;
	void update(User user) throws SQLException;
	void delete(User user) throws SQLException;
	List<User> getAll() throws SQLException;
}
