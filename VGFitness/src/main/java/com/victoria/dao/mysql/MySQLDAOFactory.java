package com.victoria.dao.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.victoria.dao.DAOFactory;
import com.victoria.dao.UserDAO;



public final class MySQLDAOFactory implements DAOFactory {
	
	private String user = "root";
	private String password = "root";
	private String url = "jdbc:mysql://localhost:3306/vgfit";
	private String driver = "com.mysql.jdbc.Driver";
	
	public MySQLDAOFactory() {
		try {
            Class.forName(driver); //register driver
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
	}
	
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}
	
	public UserDAO getUserDAO(Connection connection) {
		return new MySQLUserDAO(connection);
	}
}
