package com.victoria.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.victoria.dao.UserDAO;
import com.victoria.entity.User;

public class MySQLUserDAO implements UserDAO {
	
	private final Connection connection;
	
	public MySQLUserDAO(Connection connection) {
		this.connection = connection;
	}

	public boolean create(User user) throws SQLException {
		boolean result = false;
		connection.setAutoCommit(false);
		if(checkId(user)) {
			String query = "INSERT INTO users(idFk, pass) VALUES(?, ?);";
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setString(1, user.getId());
			statement.setString(2, user.getPass());
			statement.executeUpdate();
			result = true;
		}
		connection.commit();
		return result;
	}

	public User getByPK(String key) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public void update(User user) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public void delete(User user) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public List<User> getAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("deprecation")
	private boolean checkId(User user) throws SQLException {
		String query = "SELECT * FROM clients WHERE id = ?";
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, user.getId());
		ResultSet resultSet = statement.executeQuery(); 
		if(resultSet.next()) {
			user.setEmail(resultSet.getString("email"));
			user.setFirstName(resultSet.getString("firstName"));
			user.setSecondName(resultSet.getString("secondName"));
			Date date = resultSet.getDate("dateOfBirth");
			user.setDateOfBirth(LocalDate.of(date.getYear(), date.getMonth(), date.getDay()));
			date = resultSet.getDate("enrollmentBeg");
			user.setEnrollmentBeg(LocalDate.of(date.getYear(), date.getMonth(), date.getDay()));
			date = resultSet.getDate("enrollmentEnd");
			user.setEnrollmentEnd(LocalDate.of(date.getYear(), date.getMonth(), date.getDay()));
			user.setFreezing(resultSet.getInt("freezing"));
			user.setBonus(resultSet.getInt("bonus"));
			return true;
		}
		return false;
	}
	
}
