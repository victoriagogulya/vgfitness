<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
  <head>
   
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VG Fitness</title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <body>
            <!-- Container 1 -->
    <div class="container">
      <div class="row">
       
        <div class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#responcive-menu">
                  <span class="sr-only">Открыть навигацию</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/VGFitness">VG Fitness</a>
            </div>
            <div class="collapse navbar-collapse" id="responcive-menu">
            <ul class="nav navbar-nav">
              <li><a href="#about">О нас</a></li>
              <li><a href="#">Фитнес-услуги</a></li>
              <li><a href="/VGFitness/jsp/shedule.jsp">Расписание</a></li>
              <li><a href="#">Членство</a></li>
            </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/VGFitness/jsp/authorization.jsp">Личный кабинет</a></li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    </body>
</html>