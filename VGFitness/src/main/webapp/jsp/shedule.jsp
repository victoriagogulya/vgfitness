<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <link href="/VGFitness/css/bootstrap.css" rel="stylesheet">
    <link href="/VGFitness/css/header.css" rel="stylesheet">
    <link href="/VGFitness/css/shedule.css" rel="stylesheet">
    <link href="/VGFitness/css/footer.css" rel="stylesheet">
  </head>
    
    <body>
        
        <%@ include file="header.jsp" %>
              
                <!--  *********** Content **********  -->
        <div class="container block">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table class="table cell">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="cell">Понедельник</th>
                                <th class="cell">Вторник</th>
                                <th class="cell">Среда</th>
                                <th class="cell">Четверг</th>
                                <th class="cell">Пятница</th>
                                <th class="cell">Суббота</th>
                                <th class="cell">Воскресенье</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="cell">7:00</td>
                                <td class="cell class-rpm">
                                    RPM
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell">Yoga</td>
                                <td class="cell">
                                    Trekking - 45
                                </td>
                                <td class="cell">Yoga</td>
                                <td class="cell class-rpm">
                                    RPM
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell"></td>
                                <td class="cell"></td>
                            </tr>
                            <tr>
                                <td class="cell">8:00</td>
                                <td class="cell">
                                    ABS + Stretch
                                </td>
                                <td class="cell">Morning Express</td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell">Morning Express</td>
                                <td class="cell">ABS + Stretch</td>
                                <td class="cell"></td>
                                <td class="cell"></td>
                            </tr>
                            <tr>
                                <td class="cell">9:00</td>
                                <td class="cell">Hardcore</td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-combat">
                                    Body Combat
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell">Hardcore</td>
                                <td class="cell class-combat">
                                    Body Combat
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-rpm">
                                    RPM
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                            </tr>
                            <tr>
                                <td class="cell">10:00</td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-combat">
                                    Body Combat
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-balance">
                                    Body Balance
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-combat">
                                    Body Combat
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup></td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                            </tr>
                             <tr>
                                <td>-</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="cell">17:00</td>
                                <td class="cell class-rpm">
                                    RPM
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell">Yoga</td>
                                <td class="cell">
                                    Trekking - 45
                                </td>
                                <td class="cell">Yoga</td>
                                <td class="cell class-rpm">
                                    RPM
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell"></td>
                                <td class="cell"></td>
                            </tr>
                            <tr>
                                <td class="cell">18:00</td>
                                <td class="cell">
                                    ABS + Stretch
                                </td>
                                <td class="cell">Morning Express</td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell">Morning Express</td>
                                <td class="cell">ABS + Stretch</td>
                                <td class="cell"></td>
                                <td class="cell"></td>
                            </tr>
                            <tr>
                                <td class="cell">19:00</td>
                                <td class="cell">Hardcore</td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-combat">
                                    Body Combat
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell">Hardcore</td>
                                <td class="cell class-combat">
                                    Body Combat
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-rpm">
                                    RPM
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                            </tr>
                            <tr>
                                <td class="cell">20:00</td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-combat">
                                    Body Combat
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-balance">
                                    Body Balance
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-combat">
                                    Body Combat
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup></td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                                <td class="cell class-pump">
                                    Body Pump
                                    <sup><span class="class-lesmills">Les Mills</span></sup>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--  *********************  -->
            
         <%@ include file="/jsp/footer.jsp" %>
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="js/bootstrap.js"></script>
    </body>
</html>