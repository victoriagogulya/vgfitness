<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="/VGFitness/css/bootstrap.css" rel="stylesheet">
        <link href="/VGFitness/css/header.css" rel="stylesheet">
        <link href="/VGFitness/css/footer.css" rel="stylesheet">
    </head>
    
    <body class="wrap">
        <%@ include file="header.jsp" %>
        <div class="container" style="margin-top: 100px;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div >
                        <h3><%=request.getAttribute("message") %></h3>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="footer.jsp" %>
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="js/bootstrap.js"></script>
    </body>
</html>