<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
  </head>
    
    <body>
        <footer class="row-fluid footer">
            <div class="navbar-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <a href="#about" class="footer-references">О компании</a>
                            <a href="#" class="footer-references">Карьера в VGFitness</a>
                            <a href="#" class="footer-references">Контакты</a>
                            <hr>
                            <span class="footer-copyright">
                                Copyright &copy 2015 Фитнес-клуб VGFitness
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
    
</html>