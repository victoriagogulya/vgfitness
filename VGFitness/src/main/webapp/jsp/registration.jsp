<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="/VGFitness/css/bootstrap.css" rel="stylesheet">
        <link href="/VGFitness/css/header.css" rel="stylesheet">
        <link href="/VGFitness/css/authorization.css" rel="stylesheet">
        <link href="/VGFitness/css/registration.css" rel="stylesheet">
        <link href="/VGFitness/css/footer.css" rel="stylesheet">
    </head>
    
    <body>
        <%@ include file="header.jsp" %>
        
        <div class="container block">
            <div class="row">
                <div class="col-xs-1 col-sm-2 col-lg-2 col-md-2"></div>
                <div class="col-xs-10 col-sm-8 col-lg-8 col-md-8">
                    <div class="panel panel-default">
                        <form method="post" action="/VGFitness/RegistrationServlet">
                            <div class="panel-heading">
                            <h3 class="panel-title">Личный кабинет</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    </div>
                                    <div class="col-xs-8 col-sm-8 col-md-8 login-box">
                                        <h3>Регистрация пользователя</h3>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-user">
                                                </span>
                                            </span>
                                            <input type="text" class="form-control"
                                            name="id"
                                            placeholder="Номер клубной карты" required autofocus/>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-lock">
                                                </span>
                                            </span>
                                            <input type="password" class="form-control" 
                                            name="pass1"
                                            placeholder="Пароль" required />
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-lock">
                                                </span>
                                            </span>
                                            <input type="password" class="form-control"
                                            name="pass2"
                                            placeholder="Повторите пароль" required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                 <div class="row">
                                     <div class="col-xs-0 col-sm-0 col-md-1 col-lg-1">
                                     </div>
                                     <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="Remember"/>
                                                <div>Запомнить меня</div>
                                            </label>
                                         </div>
                                     </div>
                                     <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                                        <button type="submit" value="Apply"
                                        class="btn btn-labeled btn-success">
                                            <span class="btn-label">
                                                <i class="glyphicon glyphicon-ok">
                                                </i>
                                            </span>
                                            Зарегистрироваться
                                         </button>
                                     </div>
                                     <div class="col-xs-0 col-sm-0 col-md-1 col-lg-1">
                                     </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-1 col-sm-2 col-lg-2 col-md-2">
                </div>
            </div>
        </div>
    
            
        <%@ include file="/jsp/footer.jsp" %>
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="js/bootstrap.js"></script>
    </body>
</html>