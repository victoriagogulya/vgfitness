<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
  <head>
    <link href="/VGFitness/css/bootstrap.css" rel="stylesheet">
    <link href="/VGFitness/css/header.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
    <link href="/VGFitness/css/footer.css" rel="stylesheet">
  </head>
    
  <body class="wrap">


    <%@ include file="/jsp/header.jsp" %>

      <!-- Карусель  -->
      <div class="row">
        <div id="carousel" class="carousel slide col-md-offset-2 col-lg-offset-2  col-lg-8 col-md-8 col-sm-10" data-ride="carousel" data-interval="3000">
          
          <!-- Индикаторы слайдов  -->
          <ol class="carousel-indicators">
              <li class="active" data-target="#carousel" data-slide-to="0"></li>
              <li data-target="#carousel" data-slide-to="1"></li>
              <li data-target="#carousel" data-slide-to="2"></li>
          </ol>
          
          <!--  Слайды  -->
          <div class="carousel-inner">
              <div class="item active">
                  <img src="img/fitness1.jpg" alt="wolf">
                  <div class="carousel-caption">
                      <h3>Тренируйся</h3>
                  </div>
              </div>
              <div class="item">
                   <img src="img/fitness2.jpg" alt="cat">
                  <div class="carousel-caption">
                      <h3>Верь в себя</h3>
                  </div>
              </div>
              <div class="item">
                   <img src="img/fitness3.jpg" alt="wolf">
                  <div class="carousel-caption">
                      <h3>Не сдавайся</h3>
                  </div>
              </div>
          </div>
          
          <!-- Стрелки переключения слайдов  -->
          <a href="#carousel" class="left carousel-control" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a href="#carousel" class="right carousel-control" data-slide="next">
               <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
      </div>

 
      
    <!--  ***CONTENT***  -->
    <!--  Container 2  -->
    <div class="container-fluid">
      <!-- 1 строка  Меню Контент  -->
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
        </div>
          
         <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 about">
            <a name="about"></a>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-0">
                <img src="img/fitnessOnline.jpg" alt="fitness-online" width="200px">
            </div>

        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <div>
                 <p><strong>VG Fitness</strong> - фитнес нового поколения.<br></p>
                 <br>
                    <p>
                        Тренируйся не только в фитнес-клубе, но и <strong>online</strong>.<br>
                        Карта в фитнес-клуб, online тренировки или все вместе.<br>
                        Выбор за тобой!<br>
                    </p>
                    <p>
                        Веди свой персональный дневник.<br>
                        Общайся с тренерами и другими членами клуба.<br>
                    </p>
                    <p>Создай свой фитнес-мир с нами!</p>
            </div>
        </div>
        </div>
          
          
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-0">
        </div>
          
      </div>
    </div>
        
   <%@ include file="/jsp/footer.jsp" %>
        

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.js"></script>
  </body>
</html>

