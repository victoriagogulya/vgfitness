USE vgfit;

DROP TABLE IF EXISTS clients;
CREATE TABLE clients (
	id VARCHAR(100) NOT NULL,
    email VARCHAR(40) NOT NULL UNIQUE,
    firstName VARCHAR(40) NOT NULL,
    secondName VARCHAR(40) NOT NULL,
    dateOfBirth DATE NOT NULL,
    enrollmentBeg DATE NOT NULL,
    enrollmentEnd DATE NOT NULL,
    freezing INT(10) UNSIGNED NOT NULL,
    bonus INT UNSIGNED,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS users;
CREATE TABLE users (
	idFk VARCHAR(40) NOT NULL,
    pass VARCHAR(40) NOT NULL,
    PRIMARY KEY (idFk),
    FOREIGN KEY (idFk) REFERENCES clients(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);
	